import axios from 'axios'
import Stage from './state'
import Config from '../config/default.json'
class ApiService {

    constructor(baseUrl) {
        // super();
        // this.baseUrl = window.localStorage.getItem('BASE_URL')
        this.baseUrl = (baseUrl || Config.BASE_URL) + "/api"
    }
    setBaseUrl(baseUrl) {
        this.baseUrl = (baseUrl || Config.BASE_URL) + "/api"
    }
    getToken() {
        return Stage.token || ""
    }
    httpPost(uri, payload = {}, headers = {"Access-Control-Allow-Origin": "*"}) {
        headers.token = this.getToken()

        return new Promise((resolve, reject) => {
            axios.post(this.baseUrl + uri, payload, { headers })
            .then(res => {
                resolve(res.data)
            })
            .catch(err => {
                reject(err)
            })
        })
    }
    login(payload) {
        
    }
    logout(payload) {
        
    }
    changePassword(payload) {
        return this.httpPost("/user/change-password", payload)
    }
    getUserInfo(payload = {}) {
        return this.httpPost("/user/get-info", payload)
    }
    
    createImage(payload) {
        return this.httpPost("/image/upload", payload)
    }

    listImage(payload) {
        return this.httpPost("/user/get-images", payload)
    }

    downloadImage(payload) {
        // return this.httpPost("/user/get-info", payload)
    }
    deleteImage(payload) {
        return this.httpPost("/image/delete", payload)
    }

    newProject(payload) {
        return this.httpPost("/project/create", payload)
    }
    updateProject(payload) {
        return this.httpPost("/project/edit", payload)
    }
    listProjects(payload) {
        return this.httpPost("/project/get-list", payload)
    }
    infoProject(payload) {

    }
    deleteProject(payload) {
        return this.httpPost("/project/delete", payload)
    }
}

let apiService = new ApiService()
export default apiService