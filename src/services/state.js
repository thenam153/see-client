import Vue from 'vue'
const state = Vue.observable({
    token: localStorage.getItem('token') || "",
    // token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZFVzZXIiOjIsInVzZXJuYW1lIjoidGhlbmFtMTUzMSIsInBhc3N3b3JkIjoiYWJjMTIzNDU2NyIsImVtYWlsIjoidGhlbmFtMTUzMTJAZ21haWwuY29tIiwiYXZhdGFyIjoiIiwiY3JlYXRlZEF0IjoiMjAyMC0xMS0yN1QwNzo1MjozNi4wMDBaIiwidXBkYXRlZEF0IjoiMjAyMC0xMS0yN1QwODoxNzozOC4wMDBaIiwiaWF0IjoxNjA3MzU5NDcyLCJleHAiOjE2MDc0NDU4NzJ9.hdv_V6XrE6Fp_c17hWN9Utc9t2VY6WTdGSOiDxtUuYE",
    isLogin: false,
    defaultConfig: {
        attrs: {
            width: 1440,
            height: 1080,
            color: "rgb(255, 255, 255)"
        },
        children: {
            attrs: {
                backgroundLayer: {
                    attrs: {
                        name: "Background",
                        bgRect: {
                            draggable: false
                        },
                        children: []
                    }
                }
            },
            children: [],
            className: "LayerList"
        },
        className: "AppConfig"
        }
})

export default state;